# Python Programming for Humans [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dro248%2Fpython-programming-for-humans/master)

Welcome to Python Programming for Humans! This repository and its accompanying slides will get you started learning Python.