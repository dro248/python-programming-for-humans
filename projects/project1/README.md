Project 1 - Buying a New Car: Hybrid vs Conventional
===

Story: 
Carson, a young undergrad student, is looking to purchase a college car. 

While she knows that conventional cars come with a lower pricetag,
she is also aware that hybrid cars get great gas mileage.

Her objective is to purchase the car that will be LEAST expensive
over the next 5 years.

In the event of a tie, she chooses a hybrid.

After some research, here is what she has learned:
    
    General Info:
    - cost of gas: $3.00 per gallon
    - est. miles driven per year: 10,000

    Conventional Cars:
    - avg mileage: 25 mpg
    - 5 year resale value: 75% of full price
    - annual repair cost: $300
    
    Hybrid Cars:
    - 5 year resale value: 80% of full price
    - annual repair cost: $350

Carson decides to write a computer program to help her solve the problem.

The following information is what Carson's program requires to run:
- CONVENTIONAL car price
- HYBRID car price
- HYBRID car mileage

After this information is input, her program should output the following results:

    CONVENTIONAL car:
    - cost of fuel (over 5 years)
    - loss of value (depreciation) over 5 years
    - total cost of ownership

    HYBRID car:
    - cost of fuel (over 5 years)
    - loss of value (depreciation) over 5 years
    - total cost of ownership
